<div class="sidebar small-up-1 medium-up-2 large-up-4" role="complementary">

	<?php if ( is_active_sidebar( 'frameworque-footer' ) ) : ?>

		<!-- Widget 1 + 2 - To be set in WP Admin > Widgets -->

		<?php dynamic_sidebar( 'frameworque-footer' ); ?>

	<?php else : ?>

		<div class="alert help">

			<p><?php _e( 'Please activate some footer Widgets.', 'Frameworque' );  ?></p>

		</div>

	<?php endif; ?>

</div>
