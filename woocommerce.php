<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

		    <main id="main" class="large-10 large-centered medium-12 columns" role="main">

		    	<?php woocommerce_content(); ?>

						<?php	if ( is_shop() ) { ?>

							<div class="sidebar small-up-1 medium-up-1 large-up-1" role="complementary">

								<?php if ( is_active_sidebar( 'frameworque-woocommerce' ) ) : ?>

									<!-- Widget 1 + 2 - To be set in WP Admin > Widgets -->

									<?php dynamic_sidebar( 'frameworque-woocommerce' ); ?>

								<?php else : ?>

									<div class="alert help">

										<p><?php _e( 'Please activate some WooCommerce Widgets.', 'Frameworque' );  ?></p>

									</div>

								<?php endif; ?>

							</div>

						<?php } ?>

			</main> <!-- end #main -->

	    </div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
