<?php

/*
 *
 * TEMPLATE NAME: Lookbook
 *
 * Standard page template with description at the top.
 *
 */

get_header(); ?>

	<div id="content">

		<div class="row">

			<div class="large-8 large-centered columns">

				<br>

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>

				<?php endwhile; endif; ?>

			</div>

		</div>

		<div id="inner-content" class="row">

		    <main id="main" class="large-11 large-centered medium-centered medium-10 columns" role="main">

					<?php get_template_part( 'parts/02-components/block', 'lookbook' ); ?>

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
