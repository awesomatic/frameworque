<?php

/*
 *
 * TEMPLATE NAME: Checkout
 *
 */

get_header(); ?>

<div id="content">

	<div id="inner-content" class="medium-10 medium-centered columns">

		<main id="main" role="main" class="row">

			<div class="medium-12 columns">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>

				<?php endwhile; endif; ?>

			</div>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div>

<?php get_footer(); ?>