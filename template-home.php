<?php

/*
 *
 * TEMPLATE NAME: Home
 *
 */

get_header('home'); ?>

	<div id="content">

		<main id="main" role="main">

			<div class="row">

				<div class="medium-12 large-10 large-centered columns">

					<?php get_template_part( 'parts/02-components/woo-products', 'recent' ); ?>

				</div>

			</div>

			<div class="expanded row full-width">

				<div class="text-center">

					<?php get_template_part( 'parts/02-components/home-block', '1' ); ?>

				</div>

			</div>


			<div class="row" style="margin-top: 25px; margin-bottom: 105px;">

				<div class="large-10 large-centered columns text-center home-about">

					<?php get_template_part( 'parts/02-components/home-block', '2' ); ?>

				</div>

			</div>


			<div class="row" style="margin-top: 25px; margin-bottom: 105px;">

				<div class="large-11 large-centered columns text-center">

					<?php get_template_part( 'parts/02-components/home-block', 'usp' ); ?>

				</div>

			</div>


			<div class="expanded row full-width">

				<div class="text-center">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php // get_template_part( 'parts/loop', 'page' ); ?>

						<?php get_template_part( 'parts/02-components/img', 'content' ); ?>

					<?php endwhile; endif; ?>

				</div>

			</div>


			<div class="row" style="margin-top: 25px; margin-bottom: 75px;">

				<div class="large-10 large-centered columns text-center home-about">

					<?php get_template_part( 'parts/02-components/section', 'store' ); ?>

				</div>

			</div>

		</main>

	</div>

<?php get_footer(); ?>
