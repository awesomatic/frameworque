<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="medium-6 medium-centered large-6 large-centered columns" role="main">

				<article id="content-not-found">

					<header class="article-header">

						<h1 class="text-center"><?php _e( 'Oops! - Something went wrong.', 'Frameweworque' ); ?></h1>

					</header>

					<section class="entry-content">

						<p class="text-center">

							<?php _e( 'The article you were looking for was not found, but maybe try looking again!', 'Frameworque' ); ?>

						</p>

					</section>

					<section class="search">

					    <p><?php get_search_form(); ?></p>

					</section>

				</article>

			</main>

		</div>

	</div>

<?php get_footer(); ?>