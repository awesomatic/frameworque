<?php

/*
 *
 * FRAMEWORQUE FUNCTIONS AND DEFINITIONS
 *
 * Core and admin functions should suit every project.
 * Others are project specific.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package WordPress
 * @subpackage Frameworque
 * @since Frameworque 1.0.0
 *
 */


/*
 *
 * CORE
 *
 */


// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

require_once(get_template_directory().'/assets/functions/menu-walkers.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php');

// Add custom post type to search
require_once(get_template_directory().'/assets/functions/add-all-cpts-to-search.php');

// Related post function - no need to rely on plugins
require_once(get_template_directory().'/assets/functions/related-posts.php');

// Replace WP Gallery with Zurb Foundation Block Grid
require_once(get_template_directory().'/assets/functions/replace-wpgallery-foundation.php');

// Fix menu on search page
require_once(get_template_directory().'/assets/functions/core-fix-menu-search-page.php');

// Fix menu on search page
require_once(get_template_directory().'/assets/functions/core-split-search-results.php');


/*
 *
 * WP ADMIN
 *
 */


// Customize the WordPress login page
require_once(get_template_directory().'/assets/functions/admin-login.php');

// Remove default dashboard widgets
require_once(get_template_directory().'/assets/functions/admin-remove-dashboard-widgets.php');

// Remove default page metaboxes
require_once(get_template_directory().'/assets/functions/admin-remove-page-metaboxes.php');

// Remove default page metaboxes
require_once(get_template_directory().'/assets/functions/admin-add-featured-image-column.php');

// Custom dashboard widgets
//require_once(get_template_directory().'/assets/functions/admin-custom-dashboard-widgets.php');

// Custom admin footer
require_once(get_template_directory().'/assets/functions/admin-filter-footer-text.php');

// Adds site styles to the WordPress editor
require_once(get_template_directory().'/assets/functions/admin-editor-styles.php');

// Add duplicate posts functionality
require_once(get_template_directory().'/assets/functions/admin-duplicate-posts.php');

// Change posts per page
require_once(get_template_directory().'/assets/functions/admin-posts-per-page.php');

// Rename 'posts' post type
require_once(get_template_directory().'/assets/functions/admin-rename-post-type.php');

// Hide update notifications
require_once(get_template_directory().'/assets/functions/admin-hide-update-notifications.php');

// Show admin bar
//require_once(get_template_directory().'/assets/functions/admin-show-admin-bar.php');

// Theme option page
require_once(get_template_directory().'/assets/functions/admin-theme-option-page.php');

// User roles
require_once(get_template_directory().'/assets/functions/admin-user-roles.php');


/*
 *
 * WOOCOMMERCE & WP PLUGINS
 *
 */


// Disable default WooCommerce styles
//require_once(get_template_directory().'/assets/functions/woocommerce-disable-default-styles.php');

// Remove add to cart button
require_once(get_template_directory().'/assets/functions/woocommerce-remove-add-to-cart-button.php');

// Custom cart button text
require_once(get_template_directory().'/assets/functions/woocommerce-custom-cart-button-text.php');

// Breadcrumbs
require_once(get_template_directory().'/assets/functions/woocommerce-breadcrumbs.php');

// Remove + disable quantity fields
//require_once(get_template_directory().'/assets/functions/woocommerce-remove-quantity-fields.php');

// hide quantity fields
require_once(get_template_directory().'/assets/functions/woocommerce-hide-quantity-fields.php');

// Auto update cart when quatity changed
require_once(get_template_directory().'/assets/functions/woocommerce-auto-update-cart.php');

// Add cart as menu item
require_once(get_template_directory().'/assets/functions/woocommerce-cart-menu-item.php');

// Custom WooCommerce pagination
require_once(get_template_directory().'/assets/functions/woocommerce-custom-pagination.php');

// Products per page
require_once(get_template_directory().'/assets/functions/woocommerce-products-per-page.php');

// Swap thumbnails
//require_once(get_template_directory().'/assets/functions/woocommerce-swap-thumbnails.php');

// WooCommerce shop manager user role
require_once(get_template_directory().'/assets/functions/woo-shop-manager-user-role.php');

// WooCommerce - Show attributes under description
require_once(get_template_directory().'/assets/functions/woo-attributes.php');

// WooCommerce - Custom checkout
require_once(get_template_directory().'/assets/functions/woo-checkout.php');






remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action( 'init', 'jk_remove_wc_breadcrumbs' );
function jk_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}




add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}








//remove button archive page
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );

//remove display notice - Showing all x results
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

//remove default sorting drop-down from WooCommerce
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


/*
 *
 * SCRIPTS
 *
 */


// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Enqueue Google Fonts script
require_once(get_template_directory().'/assets/functions/enqueue-google-fonts-script.php');

// Enqueue Font Awesome script
require_once(get_template_directory().'/assets/functions/enqueue-font-awesome-script.php');

// Enqueue Owl Carousel
require_once(get_template_directory().'/assets/functions/enqueue-owl-carousel.php');

// Enqueue Fancybox
//require_once(get_template_directory().'/assets/functions/enqueue-fancybox.php');

// Google analytics
//require_once(get_template_directory().'/assets/functions/google-analytics.php');


/*
 *
 * CUSTOM
 *
 */


// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/custom-sidebars.php');

// Custom post type 'Slider'
require_once(get_template_directory().'/assets/functions/custom-post-type-slider.php');

// Custom post type 'Work'
//require_once(get_template_directory().'/assets/functions/custom-post-type-work.php');

// Custom image sizes
require_once(get_template_directory().'/assets/functions/custom-image-sizes.php');

// Swap thumbnails
require_once(get_template_directory().'/assets/functions/swap-thumbnails.php');

// Maintenance mode
//require_once(get_template_directory().'/assets/functions/maintenance-mode.php');


/*
 *
 * DEVELOPMENT
 *
 */

function fwq_wp_get_attachment_image_attributes( $attr ) {

unset($attr['title']);

 return $attr;

}

add_filter( 'wp_get_attachment_image_attributes', 'fwq_wp_get_attachment_image_attributes' );





add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}


// On the single product page, move attributes below the title.
function my_show_atts_on_product_page() {
	global $WooCommerce_Show_Attributes;
	remove_action( 'woocommerce_single_product_summary', array( $WooCommerce_Show_Attributes, 'show_atts_on_product_page' ), 25 );

	add_action( 'woocommerce_single_product_summary', array( $WooCommerce_Show_Attributes, 'show_atts_on_product_page' ), 11 );
}
add_action( 'init', 'my_show_atts_on_product_page' );



/**
 * Add an info notice instead. Let's add it after other notices with priority = 11
 *
 * Reference: https://github.com/woothemes/woocommerce/blob/master/templates/checkout/form-checkout.php
 */
add_action( 'woocommerce_before_checkout_form', 'skyverge_add_checkout_notice', 11 );
function skyverge_add_checkout_notice() {
	//wc_print_notice( __( 'A notice message instead.', 'woocommerce' ), 'notice' );
	echo '<div class="small-12 medium-12 large-9 large-centered columns">';
}


add_action( 'woocommerce_after_checkout_form', 'skyverge_close_checkout_notice', 11 );
function skyverge_close_checkout_notice() {
	//wc_print_notice( __( 'A notice message instead.', 'woocommerce' ), 'notice' );
	echo '</div">';
}






/** Remove short description if product tabs are not displayed */
function dot_reorder_product_page() {
    if ( get_option('woocommerce_product_tabs') == false ) {
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
    }
}
add_action( 'woocommerce_before_main_content', 'dot_reorder_product_page' );

/** Display product description the_content */
function dot_do_product_desc() {

    global $woocommerce, $post;

    if ( $post->post_content ) : ?>
        <div itemprop="description" class="item-description" style="border-top: dotted 0px rgba(0,0,0,0.1); margin-top: -22px;">
            <?php $heading = apply_filters('woocommerce_product_description_heading', __('Product Description', 'woocommerce')); ?>



<ul class="vertical menu accordion-menu" data-accordion-menu>
  <li>

    <ul class="menu vertical nested uitklap">
      <li style="border-bottom: 1px dotted rgba(0,0,0,0.2);"><br><?php the_content(); ?></li>
<!--       <hr> -->
    </ul>
    <a href="#" style="text-transform: uppercase; font-weight: bold; text-align: right;  color: rgba(0,0,0,0.7); font-size: 12px!important;">  <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
  </li>
</ul>



        </div>
    <?php endif;
}
add_action( 'woocommerce_single_product_summary', 'dot_do_product_desc', 25 );






/*
 * Remove excerpt from single product
 */
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
//add_action( 'woocommerce_single_product_summary', 'the_content', 20 );


/* Toggle long description */

/*
function so_43922864_add_content_open(){
    echo '<ul class="vertical menu accordion-menu" data-accordion-menu>
  <li>';
}

function so_43922864_add_content_close(){
    echo '</li><//ul>';
}

add_action( 'woocommerce_single_product_summary', 'so_43922864_add_content_open', 20 );
add_action( 'woocommerce_single_product_summary', 'the_content', 20 );
*/
//add_action( 'woocommerce_single_product_summary', 'so_43922864_add_content_close', 19 );



//show attributes after summary in product single view
	//template for this is in storefront-child/woocommerce/single-product/product-attributes.php
	//global $product;
	//echo $product->list_attributes();
