<!doctype html>

  <html class="no-js"  <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">

		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">

		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
			<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
			<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
			<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
			<meta name="msapplication-TileColor" content="#f01d4f">
			<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
	    	<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
	</head>

	<body <?php body_class(); ?>>

		<div data-sticky-container class="show-for-small-only">

			<header class="header sticky header-small" role="banner" data-sticky data-options="marginTop:0;" style="width:100%">

				<?php get_template_part( 'parts/nav-topbar', 'centered' ); ?>

			</header>

		</div>

    <!-- Preheader -->

		<section id="preheader">

			<div class="row-off">

        <div class="large-6 columns text-center">
					<?php get_template_part( 'parts/01-elements/content', 'usps' ); ?>
				</div>

				<hr class="show-for-medium-only" style="margin: 0; padding: 0;">
				<div class="large-6 columns">
					<?php get_template_part( 'parts/01-elements/woo', 'cart' ); ?>
					<?php get_template_part( 'parts/01-elements/wpml-language', 'selector' ); ?>
          <?php get_template_part( 'parts/01-elements/content', 'social' ); ?>
				</div>
			</div>

		</section>


		<hr class="show-for-small-only" style="margin: 0; padding: 0;">
		<hr class="show-for-medium-only" style="margin: 0; padding: 0;">


		<div class="row">
			<div class="small-12 columns text-center">
				<a href="<?php echo home_url(); ?>"><?php get_template_part( 'parts/01-elements/content', 'logo' ); ?></a>
			</div>
    </div>


    	<hr class="show-for-small-only">

    	<div data-sticky-container class="show-for-medium">

			<header class="header sticky" role="banner" data-sticky data-options="marginTop:0;" style="width:100%;">

				<?php get_template_part( 'parts/nav-topbar', 'centered' ); ?>

			</header>

		</div>

    <br>
