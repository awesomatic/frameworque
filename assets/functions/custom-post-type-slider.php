<?php
/* joints Custom Post Type Slider
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function codeus_custom_post_type_slider() {
	// creating (registering) the custom type
	register_post_type( 'slider', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Slider', 'codeus'), /* This is the Title of the Group */
			'singular_name' => __('slide', 'codeus'), /* This is the individual type */
			'all_items' => __('Alle slides', 'codeus'), /* the all items menu item */
			'add_new' => __('Voeg toe', 'codeus'), /* The add new menu item */
			'add_new_item' => __('Voeg nieuwe slide', 'codeus'), /* Add New Display Title */
			'edit' => __( 'Edit', 'codeus' ), /* Edit Dialog */
			'edit_item' => __('Edit slider', 'codeus'), /* Edit Display Title */
			'new_item' => __('Nieuwe slide', 'codeus'), /* New Display Title */
			'view_item' => __('Bekijk slide', 'codeus'), /* View Display Title */
			'search_items' => __('Zoek slides', 'codeus'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'codeus'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'codeus'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Dit is de slider op de homepagina', 'codeus' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-book', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'slider', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'slider', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', /* 'editor', */ /* 'author', */ 'thumbnail', /* 'excerpt', */ /* 'trackbacks',  *//* 'custom-fields', */ /* 'comments', */ 'revisions', /* 'sticky' */)
	 	) /* end of options */
	); /* end of register post type */

}

	// adding the function to the Wordpress init
	add_action( 'init', 'codeus_custom_post_type_slider');


// REMOVE POST META BOXES
function remove_my_slider_metaboxes() {

	remove_meta_box( 'slugdiv','slider','normal' ); // Slug Metabox

}

add_action('admin_menu','remove_my_slider_metaboxes');
