<?php

// REMOVE PAGE META BOXES

function fwq_remove_page_metaboxes() {

	// Custom Fields Metabox
	remove_meta_box( 'postcustom','page','normal' );

	// Excerpt Metabox
	remove_meta_box( 'postexcerpt','page','normal' );

	// Comments Metabox
	remove_meta_box( 'commentstatusdiv','page','normal' );

	remove_meta_box('commentsdiv', 'page', 'normal');

	// Talkback Metabox
	remove_meta_box( 'trackbacksdiv','page','normal' );

	// Slug Metabox
	remove_meta_box( 'slugdiv','page','normal' );

	// Author Metabox
	remove_meta_box( 'authordiv','page','normal' );

}

add_action('admin_menu','fwq_remove_page_metaboxes');

function remove_pages_count_columns($defaults) {

	unset($defaults['comments']);
	return $defaults;

}

add_filter('manage_pages_columns', 'remove_pages_count_columns');
