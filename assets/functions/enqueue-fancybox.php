<?php
/**
 * Enqueue Fancybox CSS and scripts
 *
 * @package WordPress
 * @subpackage Brandpepper Boilerplate
 * @since Brandpepper Boilerplate 0.0.5
 */

if ( ! function_exists( 'load_fancybox_css' ) ) {

	function load_fancybox_css() {

		wp_enqueue_style(
			'jquery-fancybox_css',
			get_template_directory_uri() . '/assets/css/jquery.fancybox.css',
			array(),
			false,
			'all'
		);


	}

}

if ( ! function_exists( 'load_fancybox_scripts' ) ) {

	function load_fancybox_scripts() {

		wp_enqueue_script(
			'jquery-fancybox-pack_js',
			get_template_directory_uri() . '/assets/js/jquery.fancybox.pack.js',
			array('jquery'),
			false,
			true
		);

	}

}

add_action( 'wp_enqueue_scripts', 'load_fancybox_css', 0 );
add_action( 'wp_enqueue_scripts', 'load_fancybox_scripts', 0 );


// load Foundation initialisation script in footer

if ( ! function_exists( 'fancybox_init' ) ) {

	function fancybox_init() { ?>

	<script type="text/javascript">

		jQuery(document).ready(function($) {

			$(".fancybox").fancybox( {

				openEffect	: 'elastic',
				closeEffect	: 'elastic',

				helpers: {
    overlay: {
      locked: false
    }
  }

			});

		});

	</script>

	<?php }
};

add_action( 'wp_footer', 'fancybox_init', 9999 );