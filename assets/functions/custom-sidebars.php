<?php

/*
 *
 * SIDEBARS AND WIDGETIZED AREAS
 *
 */

function joints_register_sidebars() {

/*
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'frameworque'),
		'description' => __('The first (primary) sidebar.', 'codeus'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'offcanvas',
		'name' => __('Offcanvas', 'frameworque'),
		'description' => __('The offcanvas sidebar.', 'codeus'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'codeus-shop',
		'name' => __('Shop', 'frameworque'),
		'description' => __('The shop sidebar.', 'codeus'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
*/

	register_sidebar(array(
		'id' => 'frameworque-footer',
		'name' => __('Footer', 'frameworque'),
		'description' => __('The footer sidebar.', 'frameworque'),
		'before_widget' => '<div id="%1$s" class="column widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'frameworque-woocommerce',
		'name' => __('WooCommerce', 'frameworque'),
		'description' => __('The WooCommerce sidebar.', 'frameworque'),
		'before_widget' => '<div id="%1$s" class=" widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

}
