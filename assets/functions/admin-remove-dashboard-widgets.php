<?php

/*
 *
 * REMOVE DEFAULT DASHBOARD WIDGETS
 *
 * @url https://codex.wordpress.org/Dashboard_Widgets_API#Advanced:_Removing_Dashboard_Widgets
 *
 */

function fwq_remove_dashboard_meta_boxes() {

	// Remove incoming links widget
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );

	// Remove plugins widget
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );

    // Remove primary widget
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );

    // Remove secondary widget
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );

    // Remove quick press widget
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );

    // Remove recent drafts widget
    remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );

    // Remove recent comments widget
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );

    //remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );

    //remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8

    // Remove WPML dashboard widget
    remove_meta_box( 'icl_dashboard_widget', 'dashboard', 'side' );

    // Remove welcome panel widget
    remove_action('welcome_panel', 'wp_welcome_panel');

}

add_action( 'admin_init', 'fwq_remove_dashboard_meta_boxes' );



/* Remove Yoast SEO Dashboard Widget
 * Credit: Unknown
 * Last Tested: Jun 16 2017 using Yoast SEO 4.9 on WordPress 4.8
 */
add_action('wp_dashboard_setup', 'remove_wpseo_dashboard_overview' );
function remove_wpseo_dashboard_overview() {
  // In some cases, you may need to replace 'side' with 'normal' or 'advanced'.
  remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'normal' );
}
