<?php

// Enqueues our external font awesome stylesheet

function codeus_enqueue_font_awesome_stylesheet() {

	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

}

add_action('wp_enqueue_scripts','codeus_enqueue_font_awesome_stylesheet');
