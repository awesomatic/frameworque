<?php

/*
 * Enqueue Owl Carouwel CSS and scripts
 *
 * @package WordPress
 * @subpackage Frameworque
 * @since Frameworque 0.0.1
 *
 */

// Load CSS

if ( ! function_exists( 'load_owl_carousel_css' ) ) {

	function load_owl_carousel_css() {

		wp_enqueue_style(
			'owl-carousel_css',
			get_template_directory_uri() . '/assets/css/owl.carousel.css',
			array(),
			false,
			'all'
		);

		wp_enqueue_style(
			'owl-carousel_css',
			get_template_directory_uri() . '/assets/css/owl.theme-default.css',
			array(),
			false,
			'all'
		);


	}

}

add_action( 'wp_enqueue_scripts', 'load_owl_carousel_css', 0 );


// Load scripts

if ( ! function_exists( 'load_owl_carousel_scripts' ) ) {

	function load_owl_carousel_scripts() {

		wp_enqueue_script(
			'owl-carousel_js',
			get_template_directory_uri() . '/assets/js/owl.carousel.js',
			array('jquery'),
			false,
			true
		);

	}

}

add_action( 'wp_enqueue_scripts', 'load_owl_carousel_scripts', 0 );


// load Foundation initialisation script in footer

if ( ! function_exists( 'owl_carousel_foundation_init' ) ) {

	function owl_carousel_foundation_init() { ?>

		<script type="text/javascript">

		jQuery(document).ready(function($) {

//Sort random function

var owl = $('#owl-carousel');

owl.owlCarousel({
    onInitialize: function(element){
        owl.children().sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).each(function(){
            $(this).appendTo(owl);
        });
    },

   items: 1, // The number of items you want to see on the screen.
		  	singleItem: true,
		  	loop: false,
				mouseDrag: false,
		  	lazyLoad: true
		  	//margin: 0,
		  	// autoplay:true,
		  	// autoplayTimeout:4000, // time between fades
		  	// smartSpeed: 1000,
		  	// autoplayHoverPause: false,
		  	// animateOut: 'fadeOut'



});

		  $("#owl-carousel").owlCarousel({

		  	items: 1, // The number of items you want to see on the screen.
		  	singleItem: true,
		  	loop: false,
				mouseDrag: false,
		  	lazyLoad: true
		  	//margin: 0,
		  	// autoplay:true,
		  	// autoplayTimeout:4000, // time between fades
		  	// smartSpeed: 1000,
		  	// autoplayHoverPause: false,
		  	// animateOut: 'fadeOut'

		  	//Call beforeInit callback, elem parameter point to $("#our-team")
/*
        beforeInit: function (elem) {
            random(elem);
        }
*/

		  });

		});

		</script>

	<?php }
};

add_action( 'wp_footer', 'owl_carousel_foundation_init', 9999 );
