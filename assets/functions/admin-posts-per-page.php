<?php

function fwq_admin_posts_per_page( $result, $option, $user ) {

    if ( (int)$result < 1 )

        return 75; // or whatever you want

}

add_filter( 'get_user_option_edit_page_per_page', 'fwq_admin_posts_per_page', 10, 3 );  // for pages

add_filter( 'get_user_option_edit_post_per_page', 'fwq_admin_posts_per_page', 10, 3 );  // for posts
