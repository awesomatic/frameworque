<?php

/**
 *
 * SWAP WOOCOMMERCE IMAGES
 *
 * @url http://stackoverflow.com/questions/36534105/woocommerce-product-gallery-swap-featured-image-with-selected-thumbnail
 *
 */


add_action( 'wp_enqueue_scripts', 'fwq_swap_remove_lightboxes', 99 );


  /**
   * Remove WooCommerce default prettyphoto lightbox
  */

   function fwq_swap_remove_lightboxes() {
     // Styles
     wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
     // Scripts
     wp_dequeue_script( 'prettyPhoto' );
     wp_dequeue_script( 'prettyPhoto-init' );
     wp_dequeue_script( 'fancybox' );
     wp_dequeue_script( 'enable-lightbox' );
  }


/* Customize Product Gallery */

/**
 * Click on thumbnail to view image for single product page gallery. Includes
 * responsive image support using 'srcset' attribute introduced in WP 4.4
 * @link https://make.wordpress.org/core/2015/11/10/responsive-images-in-wordpress-4-4/
 */

add_action( 'wp_footer', 'fwq_swap_gallery_override' );

function fwq_swap_gallery_override()
{
  // Only include if we're on a single work page.
  if ( is_singular( 'work' ) || is_page_template('template-store.php') ) {
  ?>
    <script type="text/javascript">
        ( function( $ ) {

            // Override default behavior
            $('.woocommerce-main-image').on('click', function( event ) {
                event.preventDefault();
            });

            // Find the individual thumbnail images
            var thumblink = $( '.thumbnails .zoom' );

            // Add our active class to the first thumb which will already be displayed
            //on page load.
            thumblink.first().addClass('active');

            thumblink.on( 'click', function( event ) {

                // Override default behavior on click.
                event.preventDefault();

                // We'll generate all our attributes for the new main
                // image from the thumbnail.
                var thumb = $(this).find('img');

                // The new main image url is formed from the thumbnail src by removing
                // the dimensions appended to the file name.
                var photo_fullsize =  thumb.attr('src').replace('-300x300','');

                // srcset attributes are associated with thumbnail img. We'll need to also change them.
                var photo_srcset =  thumb.attr('srcset');

                // Retrieve alt attribute for use in main image.
                var alt = thumb.attr('alt');

                // If the selected thumb already has the .active class do nothing.
                if ($(this).hasClass('active')) {
                    return false;
                } else {

                    // Remove .active class from previously selected thumb.
                    thumblink.removeClass('active');

                    // Add .active class to new thumb.
                    $(this).addClass('active');

                    // Fadeout main image and replace various attributes with those defined above. Once the image is loaded we'll make it visible.
                    $('.woocommerce-main-image img').css( 'opacity', '0' ).attr('src', photo_fullsize).attr('srcset', photo_srcset).attr('alt', alt).load(function() {
                        $(this).animate({ opacity: 1 });
                    });
                    return false;
                    }
                });
        } )( jQuery );
    </script>
<?php
}
}




/**
 * Responsive Image Helper Function
 *
 * @url http://aaronrutley.com/responsive-images-in-wordpress-with-acf/
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute
 */

function ar_responsive_image($image_id,$image_size,$max_width){

	// check the image ID is not blank
	if($image_id != '') {

		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );

		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );

		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';

	}
}



/**
 *
 * In WordPress 4.4 max srcset size is set to 1600px wide by default.
 * If you have images larger than that size that you’d like included in the srcset you can use this filter.
 *
 */

add_filter( 'max_srcset_image_width', 'ar_max_srcset_image_width', 10 , 2 );

function ar_max_srcset_image_width() {
	return 2200; // pixels width
}
