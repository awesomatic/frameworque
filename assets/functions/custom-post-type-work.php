<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the custom type
function codeus_custom_post_type_work() {
	// creating (registering) the custom type
	register_post_type( 'work', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Work', 'codeus'), /* This is the Title of the Group */
			'singular_name' => __('Work', 'codeus'), /* This is the individual type */
			'all_items' => __('All work', 'codeus'), /* the all items menu item */
			'add_new' => __('Add New', 'codeus'), /* The add new menu item */
			'add_new_item' => __('Add new Work', 'codeus'), /* Add New Display Title */
			'edit' => __( 'Edit', 'codeus' ), /* Edit Dialog */
			'edit_item' => __('Edit work', 'codeus'), /* Edit Display Title */
			'new_item' => __('New Work', 'codeus'), /* New Display Title */
			'view_item' => __('View Work', 'codeus'), /* View Display Title */
			'search_items' => __('Search work', 'codeus'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'codeus'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'codeus'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'codeus' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-index-card', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'work', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'work', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', '', '', '', '', 'revisions', '')
	 	) /* end of options */
	); /* end of register post type */

	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'work');
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'work');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'codeus_custom_post_type_work');


// REMOVE POST META BOXES
function remove_my_work_metaboxes() {

remove_meta_box( 'commentstatusdiv','work','normal' ); // Comments Status Metabox
remove_meta_box( 'commentsdiv','work','normal' ); // Comments Metabox
remove_meta_box( 'slugdiv','work','normal' ); // Slug Metabox

}

add_action('admin_menu','remove_my_work_metaboxes');



	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/


    /*
    	looking for custom meta boxes?
    	check out this fantastic tool:
    	https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
    */

    add_image_size( 'work', 500, 400 ); // 220 pixels wide by 180 pixels tall, hard crop mode