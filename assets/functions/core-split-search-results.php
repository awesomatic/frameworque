<?php

// Add (custom) post types to search results

function filter_search($query) {

    if ($query->is_search) {

		$query->set('post_type', array('post', 'product', 'work')); // Default post, WooCommerce products

    };

    return $query;
};

add_filter('pre_get_posts', 'filter_search');






function group_by_post_type($orderby, $query) {

global $wpdb;

	if ($query->is_search) {

	    return $wpdb->posts . '.post_type DESC';

	}

// provide a default fallback return if the above condition is not true
return $orderby;

}

add_filter('posts_orderby', 'group_by_post_type', 10, 2);
