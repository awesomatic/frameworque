<?php

# ================================================== #
# ==== Adds your styles to the WordPress editor ==== #
# ================================================== #

function codeus_add_editor_styles() {

    add_editor_style( get_template_directory_uri() . '/assets/css/style.min.css' );

}

add_action( 'init', 'codeus_add_editor_styles' );