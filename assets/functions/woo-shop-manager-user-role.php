<?php

function add_theme_caps() {

    $role = get_role( 'shop_manager' );
    $role -> add_cap( 'edit_theme_options' );

}

add_action( 'admin_init', 'add_theme_caps');
