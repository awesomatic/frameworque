<?php


/*
 *
 * FILTER ADMIN FOOTER TEXT
 *
 */

function codeus_filter_admin_footer_text() {

	_e('<span id="footer-thankyou">Created by <a href="http://jordiradstake.nl/" target="_blank">Jordi Radstake</a></span>', 'frameworque');

}

add_filter('admin_footer_text', 'codeus_filter_admin_footer_text');