<?php


/*
 *
 * ADD A SUPPORT DASHBOARD WIDGET
 *
 * @package WordPress
 * @subpackage Codeus
 * @since Codeus 1.0.0
 *
 */


function codeus_add_support_dashboard_widget() {

	wp_add_dashboard_widget(

    	'codeus_support', // Widget slug.
		'Codeus support', // Title.
        'codeus_add_support_dashboard_widget_content' // Display function.

    );

}


/*
 *
 * PUT SOME CONTENT IN THE SUPPORT WIDGET
 *
 * Create the function to output the contents of our Dashboard Widget.
 *
 */


function codeus_add_support_dashboard_widget_content() {

	echo '<h3>Veel gestelde vragen</h3>';
	echo '<p><i>Heeft u andere vragen over het werken met WordPress?';
	echo ' Stuur een e-mail naar <a href="support@codeus.nl">support@codeus.nl</a>. U wordt dan zo spoedig mogelijk geholpen.</i></p>';

}


/*** ADD A RSS DASHBOARD WIDGET ***/


 function codeus_add_rss_dashboard_widget() {

	wp_add_dashboard_widget(

		'codeus_rss', // slug
		'Codeus nieuws', // title
		'codeus_add_rss_dashboard_widget_content' // display function

	);

}


/*
 *
 * GET THE RSS FEED FOR THE RSS WIDGET
 *
 * Create the function to output the contents of our RSS Widget.
 *
 */


 function codeus_add_rss_dashboard_widget_content() {

	if (function_exists('fetch_feed')) {

		include_once(ABSPATH . WPINC . '/feed.php');               // include the required file
		$feed = fetch_feed('http://jointswp.com/feed/rss/');        // specify the source feed
		$limit = $feed->get_item_quantity(5);                      // specify number of items
		$items = $feed->get_items(0, $limit);                      // create an array of items
	}

	if ($limit == 0) echo '<div>' . __( 'The RSS Feed is either empty or unavailable.', 'codeus' ) . '</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo mysql2date(__('j F Y @ g:i a', 'codeus'), $item->get_date('Y-m-d H:i:s')); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>

	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>

	<?php }

}


add_action( 'wp_dashboard_setup', 'codeus_add_support_dashboard_widget' );

add_action('wp_dashboard_setup', 'codeus_add_rss_dashboard_widget');
