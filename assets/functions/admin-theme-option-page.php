<?php

/**
 * Generated by the WordPress Option Page generator
 * at http://jeremyhixon.com/wp-tools/option-page/
 */

class Frameworque {
	private $frameworque_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'frameworque_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'frameworque_page_init' ) );
	}

	public function frameworque_add_plugin_page() {
		add_theme_page(
			'Frameworque', // page_title
			'Frameworque', // menu_title
			'manage_options', // capability
			'frameworque', // menu_slug
			array( $this, 'frameworque_create_admin_page' ) // function
		);
	}

	public function frameworque_create_admin_page() {
		$this->frameworque_options = get_option( 'frameworque_option_name' ); ?>

		<div class="wrap">
			<h2>Frameworque</h2>
			<!-- <p>Customize jouw thema. </p> -->
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'frameworque_option_group' );
					do_settings_sections( 'frameworque-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function frameworque_page_init() {
		register_setting(
			'frameworque_option_group', // option_group
			'frameworque_option_name', // option_name
			array( $this, 'frameworque_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'frameworque_setting_section', // id
			'Social media', // title
			array( $this, 'frameworque_section_info' ), // callback
			'frameworque-admin' // page
		);

		add_settings_field(
			'facebook_0', // id
			'Facebook', // title
			array( $this, 'facebook_0_callback' ), // callback
			'frameworque-admin', // page
			'frameworque_setting_section' // section
		);

		add_settings_field(
			'instagram_1', // id
			'Instagram', // title
			array( $this, 'instagram_1_callback' ), // callback
			'frameworque-admin', // page
			'frameworque_setting_section' // section
		);

		add_settings_field(
			'pinterest_2', // id
			'Pinterest', // title
			array( $this, 'pinterest_2_callback' ), // callback
			'frameworque-admin', // page
			'frameworque_setting_section' // section
		);
	}

	public function frameworque_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['facebook_0'] ) ) {
			$sanitary_values['facebook_0'] = sanitize_text_field( $input['facebook_0'] );
		}

		if ( isset( $input['instagram_1'] ) ) {
			$sanitary_values['instagram_1'] = sanitize_text_field( $input['instagram_1'] );
		}

		if ( isset( $input['pinterest_2'] ) ) {
			$sanitary_values['pinterest_2'] = sanitize_text_field( $input['pinterest_2'] );
		}

		return $sanitary_values;
	}

	public function frameworque_section_info() {

	}

	public function facebook_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="frameworque_option_name[facebook_0]" id="facebook_0" value="%s">',
			isset( $this->frameworque_options['facebook_0'] ) ? esc_attr( $this->frameworque_options['facebook_0']) : ''
		);
	}

	public function instagram_1_callback() {
		printf(
			'<input class="regular-text" type="text" name="frameworque_option_name[instagram_1]" id="instagram_1" value="%s">',
			isset( $this->frameworque_options['instagram_1'] ) ? esc_attr( $this->frameworque_options['instagram_1']) : ''
		);
	}

	public function pinterest_2_callback() {
		printf(
			'<input class="regular-text" type="text" name="frameworque_option_name[pinterest_2]" id="pinterest_2" value="%s">',
			isset( $this->frameworque_options['pinterest_2'] ) ? esc_attr( $this->frameworque_options['pinterest_2']) : ''
		);
	}

}
if ( is_admin() )
	$frameworque = new Frameworque();

/*
 * Retrieve this value with:
 * $frameworque_options = get_option( 'frameworque_option_name' ); // Array of All Options
 * $facebook_0 = $frameworque_options['facebook_0']; // Facebook
 * $instagram_1 = $frameworque_options['instagram_1']; // Instagram
 * $pinterest_2 = $frameworque_options['pinterest_2']; // Pinterest
 */
