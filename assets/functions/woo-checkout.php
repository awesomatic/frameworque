<?php

	add_filter( 'woocommerce_checkout_fields' , 'misha_not_required_fields', 9999 );

function misha_not_required_fields( $f ) {

	unset( $f['billing']['billing_phone']['required'] );

	// the same way you can make any field required, example:
	// $f['billing']['billing_company']['required'] = true;

	return $f;
}
