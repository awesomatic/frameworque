<?php

/*
 *
 * ADD ALL CUSTOM POST TYPES TO SEARCH
 *
 * @package WordPress
 * @subpackage Codeus
 * @since Codeus 1.0.0
 *
 */


function codeus_add_cpts_to_search($query) {

	// Check to verify it's search page
	if( is_search() ) {

		// Get post types
		$post_types = get_post_types(array('public' => true, 'exclude_from_search' => false), 'objects');
		$searchable_types = array();

		// Add available post types
		if( $post_types ) {

			foreach( $post_types as $type) {

				$searchable_types[] = $type->name;

			}

		}

		$query->set( 'post_type', $searchable_types );

	}

	return $query;

}

add_action( 'pre_get_posts', 'codeus_add_cpts_to_search' );
