<?php

// Function to change "posts" to "news" in the admin side menu
function fwq_change_post_menu_label() {

    global $menu;
    global $submenu;

    $menu[5][0] = 'Nieuws';
    $submenu['edit.php'][5][0] = 'Alle berichten';
    $submenu['edit.php'][10][0] = 'Nieuw bericht';
    $submenu['edit.php'][16][0] = 'Tags';
    echo '';

}

add_action( 'admin_menu', 'fwq_change_post_menu_label' );


// Function to change post object labels to "news"
function fwq_change_post_object_label() {

    global $wp_post_types;

    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Nieuws';
    $labels->singular_name = 'Bericht';
    $labels->add_new = 'Voeg bericht toe';
    $labels->add_new_item = 'Voeg bericht toe';
    $labels->edit_item = 'Bewerk bericht';
    $labels->new_item = 'Bericht';
    $labels->view_item = 'Bekijk bericht';
    $labels->search_items = 'Zoek berichten';
    $labels->not_found = 'geen berichten gevonden';
    $labels->not_found_in_trash = 'Geen berichten gevonden in de prullenbak';
}

add_action( 'init', 'fwq_change_post_object_label' );


// REMOVE POST SUBMENUS


function fwq_remove_post_sub_menus() {

	// Remove categories submenu
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');

    // Remove tags submenu
    remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');

}

add_action('admin_menu', 'fwq_remove_post_sub_menus');


// REMOVE POST METABOXES


function fwq_remove_post_metaboxes() {

	// Categories Metabox
	remove_meta_box( 'categorydiv','post','normal' );

	// Tags Metabox
	remove_meta_box( 'tagsdiv-post_tag','post','normal' );

}

add_action('admin_menu','fwq_remove_post_metaboxes');


// REMOVE POST META BOXES
function remove_my_post_metaboxes() {

//remove_meta_box( 'authordiv','post','normal' ); // Author Metabox
remove_meta_box( 'commentstatusdiv','post','normal' ); // Comments Status Metabox
remove_meta_box( 'commentsdiv','post','normal' ); // Comments Metabox

remove_meta_box( 'postcustom','post','normal' ); // Custom Fields Metabox
remove_meta_box( 'postexcerpt','post','normal' ); // Excerpt Metabox

//remove_meta_box( 'revisionsdiv','post','normal' ); // Revisions Metabox

remove_meta_box( 'slugdiv','post','normal' ); // Slug Metabox



remove_meta_box( 'trackbacksdiv','post','normal' ); // Trackback Metabox

}

add_action('admin_menu','remove_my_post_metaboxes');




function my_columns_filter( $columns ) {

    //unset($columns['author']);
    unset($columns['tags']);
    unset($columns['categories']);
    unset($columns['comments']);
    return $columns;
}

add_filter( 'manage_edit-post_columns', 'my_columns_filter', 10, 1 );


add_action( 'load-edit.php', 'no_category_dropdown' );
function no_category_dropdown() {
    add_filter( 'wp_dropdown_cats', '__return_false' );
}
