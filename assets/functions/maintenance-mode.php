<?php

/*
 *
 * MAINTENANCE MODE
 *
 * Description:
 *
 * @link
 *
 */

function fwq_maintenance_mode() {

	if ( current_user_can( 'editor' ) || !is_user_logged_in() )  {

		wp_die('Er wordt gewerkt aan de site. Probeer het later nog eens.');

	}

}

add_action('get_header', 'fwq_maintenance_mode');
