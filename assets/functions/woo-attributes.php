<?php

/**
 * Removes the "Additional Information" tab that displays the product attributes.
 *
 * @param array $tabs WooCommerce tabs to display.
 *
 * @return array WooCommerce tabs to display, minus "Additional Information".
 */
function tutsplus_remove_product_attributes_tab( $tabs ) {

    unset( $tabs['additional_information'] );

    return $tabs;

}

add_filter( 'woocommerce_product_tabs', 'tutsplus_remove_product_attributes_tab', 100 );



//add_action( 'woocommerce_product_meta_end', 'tutsplus_list_attributes' );
// https://code.tutsplus.com/tutorials/how-to-make-woocommerce-product-attributes-more-prominent--cms-25438

add_action( 'woocommerce_single_product_summary', 'woocommerce_product_additional_information_tab',22 );

