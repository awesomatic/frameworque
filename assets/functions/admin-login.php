<?php


/*
 *
 * CUSTOMIZE THE WP ADMIN LOGIN PAGE
 *
 */


// Calling your own login css so you can style it
function fwq_login_css() {

	wp_enqueue_style( 'fwq_login_css', get_template_directory_uri() . '/assets/css/login.css', false );

}

// changing the logo link from wordpress.org to your site
function fwq_login_url() {

	return home_url();

}

// changing the alt text on the logo to show your site name
function fwq_login_title() {

	return get_option('blogname');

}

add_action( 'login_enqueue_scripts', 'fwq_login_css', 10 );

add_filter('login_headerurl', 'fwq_login_url');

add_filter('login_headertitle', 'fwq_login_title');