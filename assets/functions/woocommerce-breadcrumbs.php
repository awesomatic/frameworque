<?php

function jk_woocommerce_breadcrumbs() {

    return array(

            'delimiter'   => '',
            'wrap_before' => '<nav aria-label="You are here:" role="navigation" class="woocommerce-breadcrumb" itemprop="breadcrumb"><ul class="breadcrumbs">',
            'wrap_after'  => '</ul></nav>',
            'before'      => '<li>',
            'after'       => '</li>',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),

        );

}

add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
