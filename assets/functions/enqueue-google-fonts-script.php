<?php

// Load Google Fonts

function codeus_load_fonts() {

	wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=Chivo:400,900|PT+Sans:400,700,400italic|PT+Serif:400,700');

	wp_enqueue_style( 'googleFonts');

        }

add_action('wp_print_styles', 'codeus_load_fonts');
