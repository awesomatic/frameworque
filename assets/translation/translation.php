<?php

# =================================== #
# ==== Adding Translation Option ==== #
# =================================== #


function load_translations() {

	load_theme_textdomain( 'codeus', get_template_directory() .'/assets/translation' );
}

add_action('after_setup_theme', 'load_translations');

?>