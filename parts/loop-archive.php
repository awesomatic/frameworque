<article id="post-<?php the_ID(); ?>" <?php post_class('nieuws'); ?> role="article">
	<header class="article-header">
		<h3 class="text-center"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->

	<section class="entry-content news row" itemprop="articleBody">
		<div class="medium-3 large-2 columns">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
		</div>

		<div class="medium-9 large-10 columns">
			<?php //the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); ?>

			<?php the_content('<button class="excerpt-read-more">' . __( '<i class="fa fa-angle-double-right"></i> Read more', 'frameworque' ) . '</button>'); ?>
		</div>

		<hr>
	</section> <!-- end article section -->


	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->
</article> <!-- end article -->