<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

    <section class="entry-content" itemprop="articleBody">
		<div class="row">

			<div class="small-12 medium-5 columns">

				<!-- Post thumbnail -->

				<?php // if ( has_post_thumbnail() ) { ?>

					<?php if( get_field('afbeelding_1') ): ?>

					<a class="woocommerce-main-image"><img class="largeImage" <?php ar_responsive_image(get_field( 'afbeelding_1' ),'medium','640px'); ?> alt="text" /></a>

					<?php endif; ?>

				<?php // } else { ?>

				<!-- <p class="text-center"><?php _e( 'No image available', 'Frameworque' ); ?></p> -->

				<?php // } ?>

			</div>

			<div class="small-12 medium-7 columns">

					<p><?php the_content(); ?></p>

			</div>

		</div>

</article> <!-- end article -->
