<!-- <li><?php do_action('wpml_add_language_selector'); ?></li> -->

<ul class="lang-switcher menu show-for-medium float-right" style="padding-top: 3px;">
    <?php
        $languages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
        foreach($languages as $language){
        $flag = $language['country_flag_url'];
        $url =  $language['url'];
        $isActive = $language['active'];
        $name = $language['language_code']
    ?>

    <li><a href="<?php echo $url; ?>" <?php if($isActive == 1){ ?> class="active" <?php } ?>>
<!--             <img src="<?php echo $flag; ?>" alt="<?php echo $name; ?>" /> -->
		<?php echo $name; ?>
        </a></li>

    <?php } ?>
</ul>



<ul class="lang-switcher menu show-for-small-only float-left">
    <?php
        $languages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
        foreach($languages as $language){
        $flag = $language['country_flag_url'];
        $url =  $language['url'];
        $isActive = $language['active'];
        $name = $language['language_code']
    ?>

    <li><a href="<?php echo $url; ?>" <?php if($isActive == 1){ ?> class="active" <?php } ?>>
		<?php echo $name; ?>
        </a></li>

    <?php } ?>
</ul>