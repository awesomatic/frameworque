<?php if (has_post_thumbnail( $post->ID ) ): ?>

	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

		$image = $image[0]; ?>

<?php else : ?>

	<div class="placeholder-background-img">

		<p class="text-center"><?php _e( 'No image available', 'Frameworque' ); ?></p>

	</div>

<?php endif; ?>

<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>">

<div class="owl-carousel-img" style="background: url('<?php echo $image; ?>');">

</div>

</a>
