<ul class="menu float-right show-for-large">

	<li>
		<a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>"><i class="fa fa-facebook"></i> </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>"><i class="fa fa-instagram"></i> </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </a></li>

</ul>
