<ul class="menu show-for-medium-only" style="padding-top: 7px; font-size: 11px; display: inline-block; text-align: left;">
	<li style="padding-right: 20px;"><i class="fa fa-check"></i> <?php _e( '100% handmade', 'Frameworque' );  ?></li>
	<li style="padding-right: 20px;"><i class="fa fa-eur"></i> <?php _e( 'Pay with PayPal', 'Frameworque' );  ?></li>
	<li><i class="fa fa-truck"></i> <?php _e( 'International shipping', 'Frameworque' );  ?></li>
</ul>

<ul class="menu show-for-large" style="padding-top: 7px; font-size: 11px;">
	<li style="padding-right: 20px;"><i class="fa fa-check"></i> <?php _e( '100% handmade', 'Frameworque' );  ?></li>
	<li style="padding-right: 20px;"><i class="fa fa-eur"></i> <?php _e( 'Pay with PayPal', 'Frameworque' );  ?></li>
	<li><i class="fa fa-truck"></i> <?php _e( 'International shipping', 'Frameworque' );  ?></li>
</ul>
