<ul class="menu float-left show-for-medium-only">

	<li>
		<a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>"><i class="fa fa-facebook-square"></i> </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>"><i class="fa fa-instagram"></i> </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>"><i class="fa fa-pinterest-square"></i> </a></li>

</ul>

<ul class="menu float-right">
	<li><?php joints_woo_links(); ?></li>
</ul>