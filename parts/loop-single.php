<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <header class="article-header">
		<h3 class="text-center"><?php the_title(); ?></h3>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
	</header> <!-- end article header -->

    <section class="entry-content" itemprop="articleBody">

	    <div class="row">

		<div class="medium-4 columns">

			<?php the_post_thumbnail('full'); ?>

		</div>

		<div class="medium-8 columns">

			<?php the_content(); ?>

		</div>

	    </div>
	</section> <!-- end article section -->

	<footer class="article-footer">

		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	</footer> 		<!-- end article footer -->

	<?php //comments_template(); ?>

</article> <!-- end article -->