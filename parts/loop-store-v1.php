<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">

    <section class="entry-content" itemprop="articleBody">
		<div class="row">

			<div class="small-12 medium-5 columns">

				<!-- Post thumbnail -->

				<?php // if ( has_post_thumbnail() ) { ?>

					<?php if( get_field('afbeelding_1') ): ?>

					<a class="woocommerce-main-image"><img class="largeImage" <?php ar_responsive_image(get_field( 'afbeelding_1' ),'medium','640px'); ?> alt="text" /></a>

					<?php endif; ?>

				<?php // } else { ?>

				<!-- <p class="text-center"><?php _e( 'No image available', 'Frameworque' ); ?></p> -->

				<?php // } ?>

			</div>

			<div class="small-12 medium-7 columns">

					<p><?php the_content(); ?></p>

<!-- 					<hr> -->

					<div class="row small-up-1 medium-up-4 large-up-6 thumbnails">

						<!-- Thumb 1 -->

						<?php if( get_field('afbeelding_1') ): ?>

						<div class="column" style="margin-bottom: 15px;">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_1' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 2 -->

						<?php if( get_field('afbeelding_2') ): ?>

						<div class="column" style="margin-bottom: 15px;">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_2' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 3 -->

						<?php if( get_field('afbeelding_3') ): ?>

						<div class="column" style="margin-bottom: 15px;">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_3' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 4 -->

						<?php if( get_field('afbeelding_4') ): ?>

						<div class="column" style="margin-bottom: 15px;">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_4' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					</div>

			</div>

		</div>

</article> <!-- end article -->