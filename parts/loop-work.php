<article class="query-work-single">

	<div class="content">

		<div class="row">

			<div class="small-12 medium-4 columns">

				<!-- Post thumbnail -->

				<?php // if ( has_post_thumbnail() ) { ?>

					<?php if( get_field('afbeelding_1') ): ?>

					<a class="woocommerce-main-image"><img class="largeImage" <?php ar_responsive_image(get_field( 'afbeelding_1' ),'medium','640px'); ?> alt="text" /></a>

					<?php endif; ?>

				<?php // } else { ?>

				<!-- <p class="text-center"><?php _e( 'No image available', 'Frameworque' ); ?></p> -->

				<?php // } ?>

			</div>

			<div class="small-12 medium-8 columns">

				<!-- Page title -->

				<h3 class="text-left"><?php the_title(); ?></h3>

					<hr>

					<p><?php the_content(); ?></p>

					<!-- Gallery -->

					<?php if( get_field('afbeelding_1') ): ?>

					<h5 style="font-size: 14px; font-family: 'PT Sans'; font-weight: bold; margin-top: 65px;"><?php _e( 'Gallery', 'Frameworque' ); ?></h5>

					<?php endif; ?>



					<hr>

					<div class="row small-up-2 medium-up-3 large-up-6 thumbnails">

						<!-- Thumb 1 -->

						<?php if( get_field('afbeelding_1') ): ?>

						<div class="column">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_1' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 2 -->

						<?php if( get_field('afbeelding_2') ): ?>

						<div class="column">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_2' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 3 -->

						<?php if( get_field('afbeelding_3') ): ?>

						<div class="column">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_3' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					  	<!-- Thumb 4 -->

						<?php if( get_field('afbeelding_4') ): ?>

						<div class="column">

							<a class="zoom"><img class="my_class" <?php ar_responsive_image(get_field( 'afbeelding_4' ),'medium','640px'); ?> alt="text" /></a>

					  	</div>

					  	<?php endif; ?>


					</div>

			</div>

		</div>


	    <!-- Pagination -->

	    <hr>

			<div class="clearfix">

				<small class="float-left">

					<span><?php previous_post_link( '<span> <i class="fa fa-caret-left"></i><span> %link' ); ?></span>

				</small>

				<small class="float-right">

					<span><?php next_post_link( '%link <span> <i class="fa fa-caret-right"></i><span>' ); ?></span>

				</small>

		    </div>

		<hr>

	</div>

</article>