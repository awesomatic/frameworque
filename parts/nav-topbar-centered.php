<!-- Mobile -->

<div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
    <button class="menu-icon" type="button" data-toggle></button>
    <div class="title-bar-title"><?php _e( '', 'Frameworque' ); ?></div>
    <ul class="menu float-right show-for-small-only">
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>"><i class="fa fa-facebook-square"></i> </a></li>
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>"><i class="fa fa-instagram"></i> </a></li>
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>"><i class="fa fa-pinterest-square"></i> </a></li>
					</ul>
</div>

<!-- Large -->

<div class="top-bar" id="example-menu">
    <div class="test-test">
		<div class="row">
			<div class="small-12 medium-1 large-1 columns hide-for-small"><hr></div>
				<div class="small-12 medium-3 large-3 columns">
          <?php joints_top_nav_left(); ?>
				</div>
        <div class="small-12 medium-4 large-4 columns text-center hide-for-small">
          <a href="<?php echo home_url(); ?>" class="text-center">
          	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-logo.png" width="250" class="site-logo show-for-medium" style="margin-top:-37px; padding-bottom: 12px;">
          </a>
				</div>
        <div class="small-12 medium-3 large-3 columns">
		      <?php joints_top_nav_right(); ?>
				</div>
			<div class="small-12 medium-1 large-1 columns hide-for-small"><hr></div>
	    </div>
    </div>
</div>
