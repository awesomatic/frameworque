<div class="title-bar" data-responsive-toggle="example-menu" data-hide-for="medium">
    <button class="menu-icon" type="button" data-toggle></button>
    <div class="title-bar-title"><?php _e( '', 'Frameworque' ); ?></div>
    <ul class="menu float-right show-for-small-only">
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>"><i class="fa fa-facebook-square"></i> </a></li>
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>"><i class="fa fa-instagram"></i> </a></li>
						<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>"><i class="fa fa-pinterest-square"></i> </a></li>
					</ul>
</div>

<div class="top-bar" id="example-menu">
    <div class="test-test">
		<div class="row">
			<div class="small-1 medium-2 large-1 columns"><hr></div>

				<div class="small-10 medium-8 large-8 columns show-for-small-only">
		      <?php joints_top_nav(); ?>
				</div>

        <div class="small-10 medium-8 large-3 columns show-for-medium top_nav_large">
		      <?php joints_top_nav_left(); ?>
				</div>

        <div class="small-10 medium-8 large-4 columns show-for-medium text-center">
          <a href="<?php echo home_url(); ?>" class="text-center">
          	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/site-logo.png" width="250" class="site-logo show-for-medium">
          </a>
				</div>

        <div class="small-10 medium-8 large-3 columns show-for-medium top_nav_large">
		      <?php joints_top_nav_right(); ?>
				</div>

			<div class="small-1 medium-2 large-1 columns"><hr></div>
	    </div>
    </div>
</div>
