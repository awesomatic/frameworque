<?php

if ( is_search() ) { ?>

	<p class="byline text-left">

	<?php _e( '', 'Frameworque' ); ?> <?php the_time('j F Y') ?> <!-- by --> <?php //the_author_posts_link(); ?>    <?php //the_category(', ') ?>

	</p>

<?php } else { ?>

	<p class="byline text-center">

		<?php _e( '', 'Frameworque' ); ?> <?php the_time('j F Y') ?> <!-- by --> <?php //the_author_posts_link(); ?>    <?php //the_category(', ') ?>

	</p>

<?php } ?>