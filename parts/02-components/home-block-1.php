<div class="marketing-site-content-section">

  <div class="marketing-site-content-section-block">
    <div class="marketing-site-content-section-block-subheader text-left">
      <p><?php the_field('home_block1_paragraph'); ?></p>
    </div>
  </div>

  <div class="marketing-site-content-section-img">
    <img src="<?php the_field('home_block1_img'); ?>" alt="Atelier Judith van den Berg" />
  </div>

</div>
