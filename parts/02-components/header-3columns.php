<div class="row">
	<div class="large-3 columns">
		<?php get_template_part( 'parts/01-elements/content', 'social' ); ?>
	</div>
	<div class="large-6 columns text-center">
		<?php get_template_part( 'parts/01-elements/content', 'usps' ); ?>
	</div>
	<div class="large-3 columns">
		<?php get_template_part( 'parts/01-elements/woo', 'cart' ); ?>
		<?php get_template_part( 'parts/01-elements/wpml-language', 'selector' ); ?>
	</div>
</div>