<div class="marketing-site-content-section">
  <div class="marketing-site-content-section-img">
    <img src="<?php echo get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="Atelier Judith van den Berg" />
  </div>
  <div class="marketing-site-content-section-block">
    <div class="marketing-site-content-section-block-subheader text-left">
      <?php the_content(); ?>
      <a href="<?php the_field('link'); ?>" class="button"><?php the_field('label'); ?></a>
    </div>
  </div>
</div>
