	<div class="small-12 medium-4 large-4 columns text-center" style="text-transform: uppercase; font-size: 12px;">
    <div style="border:1px solid #eee; padding: 18px 6px 18px 6px; letter-spacing: 1px;"><i class="fa fa-check"></i> <?php _e( '100% handmade', 'Frameworque' );  ?></div>
  </div>

	<div class="small-12 medium-4 large-4 columns text-center" style="text-transform: uppercase; font-size: 12px;">
    <div style="border:1px solid #eee; padding: 18px 6px 18px 6px; letter-spacing: 1px;"><i class="fa fa-eur"></i> <?php _e( 'Pay with PayPal', 'Frameworque' );  ?></div>
  </div>

	<div class="small-12 medium-4 large-4 columns text-center" style="text-transform: uppercase; font-size: 12px;">
    <div style="border:1px solid #eee; padding: 18px 6px 18px 6px; letter-spacing: 1px;"><i class="fa fa-truck"></i> <?php _e( 'International shipping', 'Frameworque' );  ?></div>
  </div>
