<!-- <h1 class="text-center" style="padding-bottom: 0;"><?php _e( 'Store', 'Frameworque' );  ?></h1> -->

<div class="large-9 columns">
	<p>
		<img src="<?php the_field('store_img'); ?>" alt="Atelier Judith van den Berg" width="900">
	</p>
</div>

<div class="large-3 columns">

	<p><?php the_field('store_adress'); ?></p>

	<hr style="margin-bottom: 26px;">

	<p><?php the_field('store_content'); ?></p>

</div>