<!-- <h1 class="text-center" style="padding-bottom: 0;"><?php _e( 'Store', 'Frameworque' );  ?></h1> -->

<div class="large-4 columns">

	<p><?php the_field('home_block2_paragraph'); ?></p>

</div>

<div class="large-8 columns">
	<p>
		<img src="<?php the_field('home_block2_img'); ?>" alt="Atelier Judith van den Berg" width="900">
	</p>
</div>
