<?php // BAGS ?>

<h1 class="text-center" id="first" data-magellan-target="first"><?php _e( 'Collectie', 'Frameworque' );  ?></h1>

	<!-- Show for small-only -->
	<div class="hide-for-large">
		<?php echo do_shortcode('[featured_products per_page="4" columns="2" orderby="menu_order" order="asc"]'); ?>
	</div>

	<!-- Show for medium-up -->
	<div class="hide-for-small-only hide-for-medium-only">
		<?php echo do_shortcode('[featured_products per_page="3" columns="3" orderby="menu_order" order="asc"]'); ?>
	</div>

<!-- All bags button -->
<p class="text-center"><a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="button"><?php the_field('label_voor_bags_link'); ?></a></p>

<br>
<br>
<br>
