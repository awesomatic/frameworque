<ul class="menu float-left show-for-large">

	<li>
		<a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i> </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>"><i class="fa fa-instagram" aria-hidden="true"></i>
 </a></li>
	<li><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>"><i class="fa fa-pinterest-p" aria-hidden="true"></i> </a></li>

</ul>

<ul class="menu float-left show-for-medium" style="padding-left: 25px; padding-top: 7px; font-size: 12px;">
	<li style="padding-right: 20px;"><i class="fa fa-check"></i> 100% handmade</li>
	<li style="padding-right: 20px;"><i class="fa fa-check"></i> Betalen met iDEAL</li>
	<li style="padding-right: 20px;"><i class="fa fa-check"></i> Direct verzonden via PostNL</li>
</ul>