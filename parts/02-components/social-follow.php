<div class="menu social-follow">

	<div class="small-4 columns">
		<h4><a href="<?php get_template_part( 'parts/01-content/social-follow', 'facebook' ); ?>" class="fa fa-facebook"></a></h4>
	</div>


	<div class="small-4 columns">
		<h4><a href="<?php get_template_part( 'parts/01-content/social-follow', 'instagram' ); ?>" class="fa fa-instagram"></a></h4>
	</div>

		<div class="small-4 columns">
		<h4><a href="<?php get_template_part( 'parts/01-content/social-follow', 'pinterest' ); ?>" class="fa fa-pinterest-p"></a></h4>
	</div>

</div>