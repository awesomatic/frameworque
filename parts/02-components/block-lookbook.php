<?php

  $images = get_field('lookbook_gallery');

  if( $images ): ?>

      <ul class="lookbook-masonry">
          <?php foreach( $images as $image ): ?>

            <!-- Each image contains a custom field called 'link' -->
            <?php $link = get_field('product_link', $image['ID']); ?>

              <li class="overlay-image-container">
                  <a href="<?php echo $link; ?>" class="link_url">
                    <img class="overlay-image" src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <div class="caption-bg">
                      <h2 class="caption-text woocommerce-loop-product__title"><?php echo $image['caption']; ?></h2>
                      <p class="price caption-text"><i class="fa fa-shopping-cart"></i> Shop nu</p>
                    </div>
                  </a>
              </li>

          <?php endforeach; ?>
      </ul>

<?php endif; ?>
