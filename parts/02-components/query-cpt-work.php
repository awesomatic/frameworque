<?php

  $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

  $query_args = array(
      'post_type' => 'work',
      'posts_per_page' => 6,
      'paged' => $paged,
      'page' => $paged
    );

  $work_query = new WP_Query( $query_args ); ?>

  <?php if ( $work_query->have_posts() ) : ?>

    <!-- the loop -->
    <?php while ( $work_query->have_posts() ) : $work_query->the_post(); ?>

    	<article class="query-work column">

	        <div class="content">

				<?php if ( has_post_thumbnail() ) { ?>

		        	<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('work'); ?></a>

				<?php } else {

	              echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/assets/images/site-logo.png" />';
	          	}

	          ?>

	          <h3 class="text-center"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>

			  	<hr>



	        </div>

    	</article>

    <?php endwhile; ?>

    <!-- end of the loop -->


    <!-- pagination here -->

    <?php

      if (function_exists(custom_pagination)) {

        custom_pagination($work_query->max_num_pages,"",$paged);

      }

    ?>

  <?php wp_reset_postdata(); ?>

  <?php else:  ?>

  	<p><?php _e( 'Sorry, no posts matched your criteria.', 'Frameworque' );  ?></p>

  <?php endif; ?>