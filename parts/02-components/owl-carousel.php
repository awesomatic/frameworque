<div id="owl-carousel">

	<?php

		$query_args = array(

	      'post_type' => 'slider',
				'posts_per_page' => 1,
    'ignore_sticky_posts' => 1

	    );

		$slider_query = new WP_Query( $query_args ); ?>

			<?php if ( $slider_query->have_posts() ) : ?>

				<!-- the loop -->

				<?php while ( $slider_query->have_posts() ) : $slider_query->the_post(); ?>

					<?php get_template_part( 'parts/01-elements/content', 'background' ); ?>

	    		<?php endwhile; ?>

				<!-- end of the loop -->

				<?php wp_reset_postdata(); ?>

	  		<?php else:  ?>

	  			<p><?php _e( 'Sorry, no slides were found.', 'Frameworque' );  ?></p>

			<?php endif; ?>

  </div>
