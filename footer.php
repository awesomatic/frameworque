		<div class="follow-container">
			<h4 class="text-center">Follow us on social media</h4>
		</div>

			<div class="row expanded">

				<?php get_template_part( 'parts/02-components/social', 'instagram-feed' ); ?>

			</div>

			<div class="large-4 large-centered text-center">

				<?php get_template_part( 'parts/02-components/social', 'follow' ); ?>

			</div>

			<footer class="footer" role="contentinfo">

				<div id="inner-footer" class="row">

					<?php get_sidebar('footer'); ?>

				</div>

			</footer>

		<div class="post-footer-container">
			<div class="post-footer-menu">
		<?php wp_nav_menu( array( 'theme_location' => 'footer-links' ) ); ?>
			</div>

			<div class="post-footer-copyright">
				<?php get_template_part( 'parts/01-elements/content', 'copyright' ); ?>
			</div>

			<div class="post-footer-credits">
				<ul class="menu">
					<li><a href="http://jordiradstake.nl/"><?php _e( 'Website by jordiradstake.nl', 'Frameworque' );  ?></a></li>
				</ul>
			</div>
		</div>

		<?php wp_footer(); ?>

	</body>

</html>
