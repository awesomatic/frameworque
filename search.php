<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="row">

			<main id="main" class="large-10 large-centered medium-10 medium-centered columns first" role="main">

				<header>

					<h1 class="archive-title text-center"><?php _e( 'Search Results for:', 'Frameworque' ); ?>

					<?php echo esc_attr(get_search_query()); ?>

					</h1>

				</header>

				<?php if (have_posts()) :

					$last_type="";
					$typecount = 0;

					while (have_posts()) : the_post(); ?>

						<?php if ($last_type != $post->post_type) {

					    $typecount = $typecount + 1;
					    if ($typecount > 1) {

					        echo '</div><!-- close container -->'; //close type container

					    }



					    // save the post type.
					    $last_type = $post->post_type;

					    ?>

					    <?php

					    //open type container
					    switch ($post->post_type) {

					        case 'post': ?>

					          <div class="postsearch container">

						          <h4 class='text-center'><?php _e( 'Post Search results', 'Frameworque' ); ?></h4>

					        <?php break;

					        case 'work': ?>

					        	<div class="container"><h4 class='text-center'><?php _e( 'Work Search results', 'Frameworque' ); ?></h4>

					        <?php break;

					        case 'product': ?>

					        	<div class="products row small-up-1 medium-up-2 large-up-3">

								<h4 class='text-center'><?php _e( 'Shop Search results', 'Frameworque' ); ?></h4><br>

					        <?php break;

					    }
					}

						?>

						<!-- Post loop -->

					    <?php if('post' == get_post_type()) : ?>

					    	<?php get_template_part( 'parts/loop', 'search' ); ?>

						<?php endif; ?>


						<!-- Work loop -->

						<?php if('work' == get_post_type()) : ?>

					    	<?php get_template_part( 'parts/loop', 'search' ); ?>

						<?php endif; ?>


						<!-- Product loop -->

						<?php if('product' == get_post_type()) : ?>


							<?php wc_get_template_part( 'content', 'product' ); ?>


						<?php endif; ?>


					<?php endwhile; ?>


					<?php else : ?>

						<?php get_template_part( 'parts/content', 'missing' ); ?>

					<?php endif; ?>

		    </main> <!-- end #main -->

		   	<?php joints_page_navi(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>