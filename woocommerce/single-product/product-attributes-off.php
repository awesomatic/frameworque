<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-attributes.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$has_row    = false;
$alt        = 1;
$attributes = $product->get_attributes();

ob_start();

?>
</div>

<ul class="shop_attributes menu vertical">

	<?php if ( $product->enable_dimensions_display() ) : ?>

		<?php if ( $product->has_weight() ) : $has_row = true; ?>
			<li class="<?php if ( ( $alt = $alt * -1 ) === 1 ) echo 'alt'; ?>">
				<span style="font-weight: bold; font-size: 12px; text-transform: uppercase;"><?php _e( 'Weight', 'woocommerce' ) ?></span>
				<p class="product_weight"><?php echo $product->get_weight() . ' ' . esc_attr( get_option( 'woocommerce_weight_unit' ) ); ?></p>
			</li>
		<?php endif; ?>

		<?php if ( $product->has_dimensions() ) : $has_row = true; ?>
			<li class="<?php if ( ( $alt = $alt * -1 ) === 1 ) echo 'alt'; ?>">
				<span style="font-weight: bold; font-size: 12px; text-transform: uppercase;"><?php _e( 'Dimensions', 'woocommerce' ) ?></span>
				<p class="product_dimensions"><?php echo $product->get_dimensions(); ?></p>
			</li>
		<?php endif; ?>

	<?php endif; ?>

	<?php foreach ( $attributes as $attribute ) :
		if ( empty( $attribute['is_visible'] ) || ( $attribute['is_taxonomy'] && ! taxonomy_exists( $attribute['name'] ) ) ) {
			continue;
		} else {
			$has_row = true;
		}
		?>
		<li class="<?php if ( ( $alt = $alt * -1 ) == 1 ) echo 'alt'; ?>">
			<span style="font-weight: bold; font-size: 12px; text-transform: uppercase;"><?php echo wc_attribute_label( $attribute['name'] ); ?></span>
			<span><?php
				if ( $attribute['is_taxonomy'] ) {

					$values = wc_get_product_terms( $product->id, $attribute['name'], array( 'fields' => 'names' ) );
					echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				} else {

					// Convert pipes to commas and display values
					$values = array_map( 'trim', explode( WC_DELIMITER, $attribute['value'] ) );
					echo apply_filters( 'woocommerce_attribute', wpautop( wptexturize( implode( ', ', $values ) ) ), $attribute, $values );

				}
			?></span>
		</li>
	<?php endforeach; ?>

</ul>
<?php
if ( $has_row ) {
	echo ob_get_clean();
} else {
	ob_end_clean();
}
