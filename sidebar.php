<div id="sidebar1" class="sidebar large-4 medium-4 columns" role="complementary">

	<?php if ( is_active_sidebar( 'sidebar1' ) ) : ?>

		<?php dynamic_sidebar( 'sidebar1' ); ?>

	<?php else : ?>

	<div class="alert help">

		<p><?php _e( 'Please activate some Widgets.', 'Frameworque' );  ?></p>

	</div>

	<?php endif; ?>

</div>