<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

	<div class="input-group">

		<span class="input-group-label"><i class="fa fa-search"></i></span>

		<input type="search" class="search-field input-group-field large-12 columns" placeholder="<?php _e( 'Search ..', 'Frameworque' );  ?>" value="<?php echo get_search_query() ?>" type="text" name="s" title="<?php echo esc_attr_x( 'Search for:', 'Frameworque' ) ?>" />

	</div>

</form>